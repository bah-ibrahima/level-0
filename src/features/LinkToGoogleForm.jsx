import React, {memo} from 'react';

export const LinkToGoogleForm = memo((props) => {
    const {hash} = props;

    if (!hash) {
        throw Error('To access Google Form you need to provide Hash to LinkToGoogleForm component');
    }

    const link = `https://forms.gle/${hash}`

    return (
        <a className="App-link"
           href={link}
           target="_blank"
           rel="noopener noreferrer">
            Répondez aux questions du formulaire
        </a>
    );
});

